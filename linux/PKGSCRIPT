# Package Maintainers
MAINTAINERS=("Evie Viau <evie@uwueviee.live>")

# Package information
NAME="linux"
VERSION="5.16"
_MAJOR_VERSION="5"
EPOCH=0
DESC="The Linux kernel"
GRPS=("base")
URL="https://www.kernel.org/"
LICENSES=("GPL-2.0")
DEPENDS=("dracut" "kmod" "coreutils")
OPT_DEPENDS=()
MK_DEPENDS=("cpio" "pahole")
PROVIDES=("linux")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://cdn.kernel.org/pub/linux/kernel/v${_MAJOR_VERSION}.x/linux-${VERSION}.tar.xz"
"https://git.yiffos.gay/Core/patches/raw/branch/main/linux/config"
"https://git.yiffos.gay/Core/patches/raw/branch/main/linux/good_panic_message.patch")

SUM_TYPE="sha512"
SUM=("7a257dd576bc8493595ec7d6f3c9cb6e22c772a8b2dbe735d2485c4f5c56e26a08695546e7e0f1f1cd04a533f25e829361958d4da0b98bf0ba8094dd57a85aaf"
"16d1bde0a1d3ad180384f63596e4abbb468bce004871ab63588b2b1068bf65717e7d9487764f536f01aa0e31b7802b32e24dc72b580366200d32e83104d84c11"
"37f8574955ee8d440490dd756596fa256b08ea820cd85bb5844f9c1fa221731caf99d7770a362e9ff8dbb23bc4845db6a1fac2f838fb0a23748db74685b347cf")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    patch -Np1 -i ${WORKDIR}/good_panic_message.patch

    make mrproper

    cp -v "${WORKDIR}/config" .config

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    make

    make check

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    INSTALL_MOD_PATH=${BUILD_DATA_ROOT}/usr INSTALL_MOD_STRIP=1 make modules_install

    mkdir -pv ${BUILD_DATA_ROOT}/boot/

    cp -iv arch/x86/boot/bzImage ${BUILD_DATA_ROOT}/boot/vmlinuz-${VERSION}-yiffOS
    cp -iv System.map ${BUILD_DATA_ROOT}/boot/System.map-${VERSION}-yiffOS
    cp -iv .config ${BUILD_DATA_ROOT}/boot/config-${VERSION}-yiffOS

    install -d ${BUILD_DATA_ROOT}/usr/share/doc/linux-${VERSION}
    cp -r Documentation/* ${BUILD_DATA_ROOT}/usr/share/doc/linux-${VERSION}

    return 0
}