# Package Maintainers
MAINTAINERS=("Evie Viau <evie@uwueviee.live>")

# Package information
NAME="curl"
VERSION="7.81.0"
EPOCH=0
DESC="A command line tool and library for transferring data with URL syntax"
GRPS=()
URL="https://curl.se/"
LICENSES=("MIT")
DEPENDS=("openssl" "make-ca" "libidn2" "nghttp2")
OPT_DEPENDS=()
MAKE_DEPENDS=()
PROVIDES=("curl")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://curl.se/download/curl-${VERSION}.tar.xz")

SUM_TYPE="sha512"
SUM=("38355aaee38db04bb2babdc5fd7a88284580c836d15df754f42b104997dd344b7841be8e53b4fc91aea31db170a7d6967c4976833eb4bfe0d265c7275c4800df")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    grep -rl '#!.*python$' | xargs sed -i '1s/python/&3/'

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    ./configure --prefix=/usr               \
            --disable-static                \
            --with-openssl                  \
            --enable-threaded-resolver      \
            --with-ca-path=/etc/ssl/certs

    make

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    DESTDIR="${BUILD_DATA_ROOT}" make install

    rm -rf docs/examples/.deps
    find docs \( -name Makefile\* -o -name \*.1 -o -name \*.3 \) -exec rm {} \;

    install -v -d -m755 ${BUILD_DATA_ROOT}/usr/share/doc/curl-7.78.0
    cp -v -R docs/*     ${BUILD_DATA_ROOT}/usr/share/doc/curl-7.78.0

    return 0
}